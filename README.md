# MAC218-MiniEP6
## Por Arthur Pilone M. da Silva, NUSP 11795450
### Maio de 2022

Neste repositório se encontram os arquivos utilizados / criados na execução do 6º Mini EP de Técnicas de Programação 2 \[MAC218\], que trata sobre pequenos exercícios sobre a utilização do serviço 

Além de conter os arquivos expostos na tarefa e os criados durante o exercício, o repositório também contem o arquivo `history.txt` com os comandos utilizados durante a realização das duas tarefas.